package mk.iwec.selenium.demo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class HelloSeleniumIT {
	private WebDriver driver;

	@Value("${geckodriver.path}")
	private String geckodriverPath;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.gecko.driver", geckodriverPath);
		driver = new FirefoxDriver();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void helloIWEC() throws Exception {
		driver.get("https://iwec.mk");

		String actualTitle = driver.getTitle();
		assertEquals("Почетна - Интерворкс Едукативен Центар", actualTitle);

	}

	@Test
	public void helloWebDriver() throws Exception {
		driver.get("http://localhost:8080/selenium.html");

		WebElement pageHeading = driver.findElement(By.tagName("h1"));
		assertEquals("Hello Selenium", pageHeading.getText());
	}
}
